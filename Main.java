package ru.ddv.File;
import java.io.IOException;
import java.lang.String;
import java.io.File;

public class Main {

    private static String path = "D:\\1";

    public static void main(String[] args) throws IOException {

        File dir = new File(path);

        if (!dir.exists())
        {
            dir.mkdir();
        }

        FileCreator.createFile(path);
        FindSix.findSix(path);
        FindHappy.find(path);
    }
}