package ru.ddv.File;

import java.io.*;

/**
 *
 */
public class FindSix {

    public static void findSix(String dir)
    {
        int[] array = null;

        File file = new File(dir, "intdata.dat");

        try (BufferedReader in = new BufferedReader(new FileReader(file)))
        {
            array = in.lines().mapToInt(Integer::parseInt).toArray();
        }
        catch (IOException | NumberFormatException e)
        {
            e.printStackTrace();
        }

        if (array != null)
        {
            File fileSix = new File(dir, "int6data.dat");

            try
            {
                if (!fileSix.exists()) {
                    fileSix.createNewFile();
                }

                FileWriter fw = new FileWriter(fileSix);

                for (int i = 0; i < array.length; i++)
                {
                    if (array[i] >= 100000 && array[i] <= 999999)
                    {
                        fw.write(Integer.toString(array[i]) + '\n');
                        fw.flush();
                    }
                }

                fw.close();

            } catch (IOException e){
                throw new RuntimeException(e);
            }
        }
    }
}
