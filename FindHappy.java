package ru.ddv.File;
import java.io.*;

/**
 * Второй класс осуществляет чтение из файла, если он существует, и
 * ищет числа в диапазоне 100000-999999, то есть шестизначные.
 * И создает его, если нет, записывая его.
 *
 */
public class FindHappy {

    public static void find(String dir)
    {
        int[] array = null;
        File file = new File(dir, "int6data.dat");

        try (BufferedReader in = new BufferedReader(new FileReader(file)))
        {
            array = in.lines().mapToInt(Integer::parseInt).toArray();
        }
        catch (IOException | NumberFormatException e)
        {
            e.printStackTrace();
        }

        if (array != null)
        {
            File fileTxt = new File(dir, "txt6data.dat");

            try
            {
                if (!fileTxt.exists()) {
                    fileTxt.createNewFile();
                }

                FileWriter fw = new FileWriter(fileTxt);

                for (int i = 0; i < array.length; i++) {
                    int[] three = new int[2];
                    three[0] = array[i] / 1000;
                    three[1] = array[i] % 1000;

                    int sum[] = {0, 0};

                    for (int j = 0; j < three.length; j++) {
                        while (three[j] > 0) {
                            sum[j] += three[j] % 10;
                            three[j] /= 10;
                        }
                    }

                    if (sum[0] == sum[1]) {
                        fw.write(Integer.toString(array[i]) + '\n');
                        fw.flush();
                    }
                }
                fw.close();

            }
            catch (IOException e){
                throw new RuntimeException(e);
            }
        }
    }
}
