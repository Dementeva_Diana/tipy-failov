package ru.ddv.File;
import java.io.*;
import java.util.Random;

/**
 * Этот класс создает intdata.dat и записывает туда 1000 чисел от 0 до 999999.
 * Выполнила студентка группы 15.18
 * Дементьева Диана.
 */
public class FileCreator {

    public static void createFile(String dir)
    {
//        Random randInt = new Random();
        int n = 1000;
        int[] array = new int[n];

        for(int i = 0; i < n; i++)
        {
//            array[i] = randInt.next+Int(999999 + 1);
            array[i] = (int) (Math.random()*(999999));
        }

        File created = new File(dir, "intdata.dat");

        try {
            if (!created.exists()) {
                created.createNewFile();
            }

            FileWriter fw = new FileWriter(created);

            for (int i = 0; i < array.length; i++) {
                fw.write(Integer.toString(array[i]));
                fw.append('\n');
                fw.flush();
            }

            fw.close();
        } catch (IOException e){
            throw new RuntimeException(e);
        }
    }
}
